if (process.cwd() !== __dirname) {
	throw new Error('Process cwd must be equal to this file\'s directory!');
}

const { resolve: pathResolve } = require('path');
require('dotenv').config({ path: pathResolve(__dirname, '.env') });

const loadData = require('./lib/load-data');
const Search = require('./lib/search');
const startApp = require('./lib/start-app');

/*  eslint-disable dot-notation */
const ENV = process.env['NODE_ENV'];
const port = Number(process.env['PORT']);
const dataFile = process.env['DATA_FILE'];
/*  eslint-enable dot-notation */

let data;
let search;

console.log('ENV', ENV);

console.log('Loading data...');
loadData(pathResolve(__dirname, dataFile)).
	then(data_ => {
		console.log('Data loaded successfully');
		data = data_;
		search = new Search(data);
	}).
	then(() => console.log('Starting app...')).
	then(() => startApp(port)).
	then(app => {
		console.log(`Listening on port ${port}`);
		if ('production' === ENV) {
			console.log('--==O==--');
		}

		app.post(/.*/, (req, res) => {
			const { nh: noHeader, id, q: query } = req.body;

			const result = {};

			let success = true;

			if ('string' === typeof id && 0 < id.length) {
				result.entry = search.getById(id);
			} else if ('string' === typeof query && 0 < query.length) {
				result.entries = search.find(query);
			} else {
				result.error = 'Invalid request';
				success = false;
			}

			if (success && null == noHeader) {
				result.header = [
					data.fieldsEn, data.fieldsRu
				];
			}

			res.type('application/json; charset=UTF-8');
			res.send(JSON.stringify(result));
		});
	}).

	catch(err => console.error(err));
