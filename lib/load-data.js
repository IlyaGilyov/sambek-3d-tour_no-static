const { createReadStream } = require('fs');
const { basename } = require('path');
const { pipeline } = require('stream');
const { createGunzip } = require('zlib');
const { extract } = require('tar-stream');
const csv = require('@fast-csv/parse');


const loadCSV = dataFile => new Promise((resolve, reject) => {
	const csvData = new Map();

	const untar = extract();
	untar.on('entry', (header, stream, next) => {
		const name = basename(header.name, '.csv');

		stream.pipe(csv.parse()).
			on('data', row => {
				let rows = csvData.get(name);
				if (undefined === rows) {
					rows = [];
					csvData.set(name, rows);
				}

				rows.push(row);
			}).
			on('end', () => next());
	});

	pipeline(
		[
			createReadStream(dataFile),
			createGunzip(),
			untar
		],
		err => {
			if (err) {
				reject(err);
				return;
			}

			resolve(csvData);
		}
	);
});

const loadData = dataFile => loadCSV(dataFile).
	then(csvData => {
		const [fieldsEn, fieldsRu] = csvData.get('fields');
		const locations = csvData.get('locations')[0];
		const names = csvData.get('names')[0];
		const middleNames = csvData.get('middle-names')[0];
		const surnames = csvData.get('surnames')[0];
		const places = csvData.get('places')[0];
		const ranks = csvData.get('ranks')[0];

		const srcEntries = csvData.get('entries');

		const entries = [];

		for (const entrySrc of srcEntries) {
			const id = entrySrc[0];
			const entry = { id };
			for (let i = 1; i < entrySrc.length; ++i) {
				const field = fieldsEn[i - 1];
				let value = entrySrc[i];

				if ('' === value) {
					continue;
				}

				switch (field) {
					case 'name':
					case 'middle_name':
					case 'surname': {
						const values = 'name' === field ?
							names :
							'middle_name' === field ?
								middleNames :
								surnames;
						value = values[+value];
						break;
					}

					case 'birth_place':
					case 'last_place_of_service':
					case 'drop_out_place': {
						value = places[+value];
						break;
					}

					case 'birth_date':
					case 'drop_out_date':
					case 'death_date': {
						value = value.split('.').map(v => Number(v) || null);
						break;
					}

					case 'rank': {
						value = ranks[+value];
						break;
					}

					case 'location': {
						value = locations[+value];
					}
				}

				entry[field] = value;
			}

			entries.push(entry);
		}

		return {
			fieldsEn, fieldsRu,
			names, middleNames, surnames,
			// locations, places, ranks,
			entries
		};
	});

module.exports = loadData;
