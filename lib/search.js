const { distance: levenshteinDistance } = require('fastest-levenshtein');

class Search {
	constructor({
		names,
		middleNames,
		surnames,
		entries
	}) {
		this._names = names;
		this._middleNames = middleNames;
		this._surnames = surnames;
		this._entries = entries;
	}

	find(str, limit = 12) {
		const searchEntries = str.
			trim().toLowerCase().replace(/[^а-я\s]+/g, '').split(/\s+/).
			filter(str => 0 < str.length);

		if (0 === searchEntries.length) {
			return [];
		}

		const namesList = this._names;
		const middleNamesList = this._middleNames;
		const surnamesList = this._surnames;

		const names = new Set();
		const middleNames = new Set();
		const surnames = new Set();

		for (let seIdx = 0; seIdx < searchEntries.length; ++seIdx) {
			const se = searchEntries[seIdx];

			let mName;
			let nameDist = Infinity;
			for (let i = 0; i < namesList.length; ++i) {
				const lName = namesList[i];
				const dist = levenshteinDistance(se, lName);
				if (dist < nameDist) {
					mName = lName;
					nameDist = dist;
					if (0 === dist) {
						break;
					}
				}
			}

			let mMiddleName;
			let middleNameDist = Infinity;
			for (let i = 0; i < middleNamesList.length; ++i) {
				const lMiddleName = middleNamesList[i];
				const dist = levenshteinDistance(se, lMiddleName);
				if (dist < middleNameDist) {
					mMiddleName = lMiddleName;
					middleNameDist = dist;
					if (0 === dist) {
						break;
					}
				}
			}

			let mSurname;
			let surnameDist = Infinity;
			for (let i = 0; i < surnamesList.length; ++i) {
				const lSurname = surnamesList[i];
				const dist = levenshteinDistance(se, lSurname);
				if (dist < surnameDist) {
					mSurname = lSurname;
					surnameDist = dist;
					if (0 === dist) {
						break;
					}
				}
			}

			if (nameDist < se.length && nameDist <= middleNameDist && nameDist <= surnameDist) {
				names.add(mName);
			}

			if (middleNameDist < se.length && middleNameDist <= nameDist && middleNameDist <= surnameDist) {
				middleNames.add(mMiddleName);
			}

			if (surnameDist < se.length && surnameDist <= nameDist && surnameDist <= middleNameDist) {
				surnames.add(mSurname);
			}
		}

		if (
			0 === names.size &&
			0 === middleNames.size &&
			0 === surnames.size
		) {
			return [];
		}

		const exact = [];
		const sn = [];
		const npsn = [];
		const rest = [];

		for (
			let i = 0,
				cprName = 0 !== names.size,
				cprMiddleName = 0 !== middleNames.size,
				cprSurname = 0 !== surnames.size,
				entries = this._entries;
			i < entries.length; ++i
		) {
			const entry = entries[i];
			const hasName = cprName && names.has(entry.name);
			const hasMiddleName = cprMiddleName && middleNames.has(entry.middle_name);
			const hasSurname = cprSurname && surnames.has(entry.surname);
			if (hasName || hasMiddleName || hasSurname) {
				if (hasName && hasMiddleName && hasSurname) {
					exact.push(entry);
				} else if (hasName && hasSurname) {
					npsn.push(entry);
				} else if (hasSurname) {
					sn.push(entry);
				} else {
					rest.push(entry);
				}

				if (0 !== exact.length && exact.length + npsn.length + rest.length >= limit) {
					break;
				}
			}
		}

		const res = exact.concat(sn, npsn, rest);

		return limit > 0 && res.length > limit ? res.slice(0, limit) : res;
	}

	getById(id) {
		for (let i = 0, entries = this._entries; i < entries.length; ++i) {
			const entry = entries[i];
			if (id === entry.id) {
				return entry;
			}
		}

		return null;
	}
}

module.exports = Search;
