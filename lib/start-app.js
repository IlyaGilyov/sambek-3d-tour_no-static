const express = require('express');
const morgan = require('morgan');
const cors = require('cors');

const ENV = process.env['NODE_ENV']; // eslint-disable-line dot-notation
if ('production' !== ENV && 'development' !== ENV) {
	throw new Error(`Invalid environment type variable value: "${ENV}". Acceptable values: "development", "production". Check dotenv file.`);
}

const startApp = port => new Promise((resolve, reject) => {
	if (!isFinite(port) || port <= 0) {
		reject(new TypeError(`Invalid port: ${port}`));
		return;
	}

	const app = express();
	app.disable('x-powered-by');

	app.use(morgan(
		'production' === ENV ? 'combined' : 'dev',
		{ /* morgan options */ }
	));

	app.use(express.urlencoded({
		extended: false,
		limit: '1kb'
	}));

	app.use(cors());

	app.get(/.*/, (req, res) => res.status(405).send('Method Not Allowed'));

	app.listen(port, () => resolve(app));
});

module.exports = startApp;
