const path = require('path');
const fs = require('fs');

const prompt = require('prompt-sync')();
const request = require('sync-request');

const ora = require('ora');

const tar = require('tar-stream');


const cyclicPrompt = (
	msgs,
	validate = (/* ans, msgs, idx */) => true,
	add = ' > '
) => {
	if (!Array.isArray(msgs)) {
		msgs = [msgs];
	} else if (0 === msgs.length) {
		msgs.push('No message');
	}

	let ans;
	for (let i = 0; ; ++i) {
		ans = prompt(
			`${msgs[Math.min(i, msgs.length - 1)]}${add}`
		);

		if (null == ans) {
			process.exit(1);
		}

		if (true === validate(ans, msgs, i)) {
			break;
		}
	}

	return ans;
};


const searchServiceURL = cyclicPrompt(
	'Enter search service URL',
	ans => {
		let url;
		try {
			url = new URL(ans);
		} catch (e) {
			console.warn('Invalid URL:', ans);
			return false;
		}

		try {
			JSON.parse(String(request('POST', String(url), {
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				},
				body: 'nh&id=100000',
				timeout: 3e4
			}).getBody()));
		} catch (e) {
			console.warn('Unable to connect search service using URL:', String(url));

			if (
				'y' !== cyclicPrompt(
					'Do you want to continue anyway? [y/N]',
					(ans, msgs) => {
						if (!['', 'y', 'n'].includes(ans.trim().toLowerCase())) {
							msgs.push('Type "y" or "n" or leave blank');
							return false;
						}

						return true;
					}
				).trim().toLowerCase()
			) {
				process.exit(1);
			}
		}

		return true;
	}
);

const targetDir = path.resolve(
	process.cwd(),
	cyclicPrompt(
		'Enter target directory',
		ans => {
			const dirPath = path.resolve(process.cwd(), ans);
			if (process.cwd() === dirPath) {
				console.warn(dirPath, 'is current working directory');
				return false;
			}

			try {
				fs.accessSync(dirPath, fs.constants.F_OK | fs.constants.W_OK);
			} catch (e) {
				console.warn(dirPath, 'does not exist or access denied');
				return false;
			}

			const stat = fs.lstatSync(dirPath);
			if (!stat.isDirectory()) {
				console.warn(dirPath, 'is not a directory');
				return false;
			}

			return true;
		}
	)
);

console.info(`
  Search service URL: ${searchServiceURL}
  Target directory: ${targetDir}
`);

if (
	'yes' !== cyclicPrompt(
		'Is this information correct? (yes|no)',
		(ans, msgs) => {
			if (!['yes', 'no'].includes(ans)) {
				msgs.push('Type "yes" or "no"');
				return false;
			}

			return true;
		}
	)
) {
	process.exit(0);
}

const staticTarPath = path.join(process.cwd(), 'static.tar');

const searchServiceFormFileNames = [
	'veteran-search-form.js',
	'veteran-search-form.min.js'
];

const checkDir = filePath => {
	const dirPath = path.dirname(filePath);
	let exists = false;
	try { fs.accessSync(dirPath); exists = true; } catch (e) { /**/ }
	if (!exists) {
		fs.mkdirSync(dirPath, { recursive: true });
	}
};

const untar = () => new Promise((resolve, reject) => {
	const spinner = ora('Copying').start();

	const extract = tar.extract();
	extract.on('entry', (header, stream, next) => {
		if ('file' === header.type) {
			const dstPath = path.resolve(targetDir, header.name);
			const fileName = path.basename(dstPath);

			spinner.text = `Copying ${header.name} to ${dstPath}`;

			checkDir(dstPath);

			stream.on('end', () => next());

			if (searchServiceFormFileNames.includes(fileName)) {
				const chunks = [];
				stream.on('data', chunk => chunks.push(chunk));
				stream.on('end', () =>
					fs.writeFileSync(
						dstPath,
						String(Buffer.concat(chunks)).
							replace(
								/const\s+SEARCH_SERVICE_URL\s*=\s*\'[^\']*\'\s*;/,
								`const SEARCH_SERVICE_URL = '${searchServiceURL}';`
							)
					)
				);
			} else {
				stream.pipe(fs.createWriteStream(dstPath));
			}
		} else {
			stream.on('end', () => next());
			stream.resume();
		}
	});

	extract.on('error', err => reject(err));

	extract.on('finish', () => {
		spinner.succeed('Done');
		resolve();
	});

	fs.createReadStream(staticTarPath).pipe(extract);
});

untar().
	catch((err) => console.error(err));
