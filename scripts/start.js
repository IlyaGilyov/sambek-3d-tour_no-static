const dotenvCfg = require('dotenv').config();

if (null != dotenvCfg.error) {
	console.warn('You should setup you dotenv file and make it readable to start the app');
	throw dotenvCfg.error;
}

const { exec, fork } = require('child_process');

const path = require('path');
const fs = require('fs');

const prompt = require('prompt-sync')();

/* eslint-disable dot-notation */
console.log('APP_NAME', process.env['APP_NAME']);
console.log('ENV', process.env['NODE_ENV']);
console.log('PORT', process.env['PORT']);
console.log('DATA_FILE', process.env['DATA_FILE']);
console.log('LOGS_DIR', process.env['LOGS_DIR']);

const APP_NAME = process.env['APP_NAME'];
const ENV = process.env['NODE_ENV'];
const LOGS_DIR = process.env['LOGS_DIR'];
/* eslint-enable dot-notation */

if ('production' === ENV) {
	const checkLogsDir = () => new Promise((resolve, reject) => {
		let logsDir;

		if (null != LOGS_DIR) {
			logsDir = path.resolve(LOGS_DIR);
		} else {
			logsDir = path.join(process.cwd(), 'logs');
		}

		fs.access(logsDir, err => {
			if (err) {
				console.log(`Logs dir "${logsDir}" does not exist`);
				let ans;
				for (let msg = 'Do you want to create it? (yes|no) > '; ;) {
					ans = prompt(msg);

					if ('yes' !== ans && 'no' !== ans) {
						msg = 'Please type "yes" or "no" > ';
						continue;
					}

					break;
				}

				if ('no' === ans) {
					reject(new Error('Logs dir is not set. Unable to proceed'));
					return;
				}

				fs.mkdir(logsDir, { recursive: true }, err => {
					if (err) {
						reject(err);
						return;
					}

					resolve(logsDir);
				});

				return;
			}

			resolve(logsDir);
		});
	});

	const env = Object.assign({}, process.env);
	delete env['NODE_ENV']; // eslint-disable-line dot-notation

	const checkPM2 = () => new Promise(
		(resolve, reject) =>
			exec('npm ls -g --depth=0', { env: env }, (err, stdout/*, stderr*/) => {
				if (err) {
					reject(err);
					return;
				}

				console.log(stdout);

				resolve(/^\W+pm2@/m.test(stdout));
			})
	);

	const intstallPM2 = () => new Promise(
		(resolve, reject) =>
			exec('npm i pm2 -g', { env: env }, (err, stdout/*, stderr*/) => {
				if (err) {
					reject(err);
					return;
				}

				// console.log(stdout);

				if (/^\W+pm2@/m.test(stdout)) {
					console.log('PM2 installed');

					resolve();
					return;
				}

				reject(new Error('Failed to install PM2'));
			})
	);

	const getJList = () => new Promise((resolve, reject) => {
		const check = attemptsLeft => exec('pm2 jlist', { env: env }, (err, stdout/*, stderr*/) => {
			if (err) {
				reject(err);
				return;
			}

			if (0 >= attemptsLeft) {
				reject(new Error('Max attempts to read json apps list exceeded'));
				return;
			}

			let apps;

			try {
				apps = JSON.parse(stdout);
			} catch (e) { /**/ }

			if (null == apps) {
				setTimeout(() => check(attemptsLeft - 1), 1000);
				return;
			}

			resolve(apps);
		});
		check(5);
	});

	const stopIfRunning = () => getJList().
		then(apps => new Promise((resolve, reject) => {
			let running = false;

			for (const app of apps) {
				if (APP_NAME === app.name) {
					running = true;
					break;
				}
			}

			if (running) {
				console.log('App is running. Stopping');
				exec(`pm2 delete ${APP_NAME}`, { env: env }, (err, /*stdout, stderr*/) => {
					if (err) {
						reject(err);
						return;
					}

					resolve();
				});

				return;
			}

			resolve();
		}));

	const waitForFile = (file, interval = 100, maxChecks = 100) => new Promise((resolve, reject) => {
		const check = () => fs.access(file, err => {
			if (err) {
				if (0 >=-- maxChecks) { // eslint-disable-line space-infix-ops
					reject(new Error('File max checks exceeded'));
				} else {
					setTimeout(() => check(), interval);
				}

				return;
			}

			resolve();
		});
		check();
	});

	const initOutput = outputFile =>
		waitForFile(outputFile).
			then(() => new Promise((resolve/*, reject*/) => {
				let output = String(fs.readFileSync(outputFile));
				if ('' !== output) {
					console.log(output);
				}

				const listener = () => {
					const contents = String(fs.readFileSync(outputFile));
					const addOutput = contents.slice(output.length);
					if ('' !== addOutput) {
						console.log(addOutput);

						output += addOutput;
						if (output.includes('--==O==--')) {
							fs.unwatchFile(outputFile, listener);
							resolve();
						}
					}
				};

				fs.watchFile(outputFile, { interval: 300 }, listener);
			}));

	let logsDir;

	checkLogsDir().
		then(logsDir_ => {
			logsDir = logsDir_;
			console.log('Logs dir:', logsDir);
		}).
		then(() => checkPM2()).
		then(res => {
			if (true === res) {
				console.log('PM2 is installed, skipping installation');
				return;
			}

			console.log('PM2 is not installed, installing');
			return intstallPM2();
		}).

		then(() => stopIfRunning()).

		then(() => {
			const ts = Date.now();
			const outLogFile = path.join(logsDir, `out-${ts}.log`);
			const errLogFile = path.join(logsDir, `error-${ts}.log`);

			const cmd = 'pm2 start index.js ' +
				`--name ${APP_NAME} ` +
				`--output ${outLogFile} ` +
				`--error ${errLogFile}`;

			return new Promise(
				(resolve, reject) =>
					exec(cmd, (err/*, stdout, stderr*/) => {
						if (err) {
							reject(err);
							return;
						}

						// console.log(stdout);
						resolve();
					})
			).
				then(() => initOutput(outLogFile)).
				then(() => {
					console.log('App started successfuly');
					console.log(
						'For process management use `pm2 list` or `pm2 monit`\n' +
							'\tor look pm2.keymetrics.io for more information'
					);

					process.exit(0);
				});
		}).

		catch(err => console.error(err));
} else if ('development' === ENV) {
	console.log('In development environment you should really just run `node index.js`');
	fork('index.js');
} else {
	console.error(new Error(`Unknown env variable value: ${ENV}`));
}

